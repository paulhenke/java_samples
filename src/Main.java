import aufgabe2.DistanceApplication;
import aufgabe3.Aufgabe3;
import aufgabe4.Aufgabe4;

public class Main {

    public static void main(String[] args) {
        System.out.println("Aufgabe 2 - Point");
        DistanceApplication.main();
        System.out.println();

        System.out.println("Aufgabe 3 - Boundingbox");
        Aufgabe3.main();
        System.out.println();

        System.out.println("Aufgabe 4 - Polyline&gone");
        Aufgabe4.main();
        System.out.println();
    }
}
