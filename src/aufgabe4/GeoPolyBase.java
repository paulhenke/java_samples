package aufgabe4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class GeoPolyBase implements Geometry {

    private final String name;
    protected final List<GeoPoint> points;

    protected GeoPolyBase(String name, Collection<GeoPoint> points) {
        this.name = name;
        this.points = new ArrayList<>(points);
    }

    public void addGeoPoint(GeoPoint p) {
        points.add(p);
    }

    public void addGeoPoint(GeoPoint p, int index) {
        points.add(index, p);
    }

    public String getName() {
        return name;
    }

    protected double calcLength() {
        double result = 0;
        for (int i = 0; i < points.size() - 1; i++) {
            result = result + GeoUtil.distance(points.get(i), points.get(i + 1));
        }

        return result;
    }

    @Override
    public BoundingBox getBoundingBox() {
        return new BoundingBox(points);
    }
}
