package aufgabe4;

import java.util.Collection;

public class GeoPolygon extends GeoPolyBase implements Geometry {
    protected GeoPolygon(String name, Collection<GeoPoint> points) {
        super(name, points);
    }

    public double getPerimeter() {
        GeoPoint first = super.points.get(0);
        GeoPoint last = super.points.get(super.points.size() - 1);
        return super.calcLength() + GeoUtil.distance(first, last);
    }
}
