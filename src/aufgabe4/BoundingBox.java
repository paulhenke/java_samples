package aufgabe4;

import java.util.Collection;
import java.util.Comparator;

public class BoundingBox {
    private final GeoPoint maxPoint;
    private final GeoPoint minPoint;

    public BoundingBox(Collection<GeoPoint> points) {
        maxPoint = points.stream().max(Comparator.comparing(GeoPoint::getDistanceToZero)).get();
        minPoint = points.stream().min(Comparator.comparing(GeoPoint::getDistanceToZero)).get();
    }

    public GeoPoint getMaxPoint() {
        return maxPoint;
    }

    public GeoPoint getMinPoint() {
        return minPoint;
    }

    public double getWidth() {
        return maxPoint.getX() - minPoint.getX();
    }

    public double getHeight() {
        return maxPoint.getY() - minPoint.getY();
    }
}
