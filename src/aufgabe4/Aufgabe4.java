package aufgabe4;

import java.util.Arrays;
import java.util.List;

public class Aufgabe4 {

    public static void main() {
        List<GeoPoint> points = Arrays.asList(
                new GeoPoint(0,0),
                new GeoPoint(5,1),
                new GeoPoint(3,2),
                new GeoPoint(2,4)
        );

        GeoPolyline line = new GeoPolyline("line", points);
        GeoPolygon gon = new GeoPolygon("gon", points);

        System.out.println("Polyline named " + line.getName() + " has length: " + line.getLenght());
        System.out.println("Polygone named " + gon.getName() + " has perimeter: " + gon.getPerimeter());

        GeoPoint p10 = new GeoPoint(10, 10);
        line.addGeoPoint(p10);
        gon.addGeoPoint(p10);

        System.out.println("Added Point " + p10);
        System.out.println("Polyline named " + line.getName() + " has length: " + line.getLenght());
        System.out.println("Polygone named " + gon.getName() + " has perimeter: " + gon.getPerimeter());

    }
}
