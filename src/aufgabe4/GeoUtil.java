package aufgabe4;

public abstract class GeoUtil {
    public static double distance(GeoPoint p1, GeoPoint p2) {
        double dx = p1.getX() - p2.getX();
        double dy = p1.getY() - p2.getY();
        return Math.sqrt(dx * dx + dy * dy);
    }
}
