package aufgabe4;

import java.util.Collection;

public class GeoPolyline extends GeoPolyBase implements Geometry {

    public GeoPolyline(String name, Collection<GeoPoint> points) {
        super(name, points);
    }

    public double getLenght() {
        return super.calcLength();
    }
}
