package aufgabe4;

public interface Geometry {
    BoundingBox getBoundingBox();
}
