package aufgabe2;

public class DistanceApplication {

    public static void main() {
        Point p1 = new Point(1, 1);
        Point p2 = new Point(5, 3);

        double distance = GeoUtil.distance(p1, p2);

        System.out.println("Distanz zwischen " + p1 + " und " + p2 + " beträgt: " +distance );
    }
}
