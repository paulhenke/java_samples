package aufgabe2;

import java.util.Locale;

public class Point {

    private static final Point point0 = new Point(0,0);

    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return String.format(Locale.ROOT, "(%.2f,%.2f)", x, y);
    }

    public double getDistanceToZero() {
        return GeoUtil.distance(point0, this);
    }
}
