package aufgabe3;

import aufgabe2.Point;

import java.util.Arrays;
import java.util.Comparator;

public class BoundingBox {

    private final Point maxPoint;
    private final Point minPoint;

    public BoundingBox(Point ...points) {
        maxPoint = Arrays.stream(points).max(Comparator.comparing(Point::getDistanceToZero)).get();
        minPoint = Arrays.stream(points).min(Comparator.comparing(Point::getDistanceToZero)).get();
    }

    public Point getMaxPoint() {
        return maxPoint;
    }

    public Point getMinPoint() {
        return minPoint;
    }

    public double getWidth() {
        return maxPoint.getX() - minPoint.getX();
    }

    public double getHeight() {
        return maxPoint.getY() - minPoint.getY();
    }
}
