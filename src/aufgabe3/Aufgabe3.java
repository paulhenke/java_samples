package aufgabe3;

import aufgabe2.Point;

public abstract class Aufgabe3 {

    public static void main() {
        Point p1 = new Point(2, 1);
        Point p2 = new Point(5, 3);
        Point p3 = new Point(10, 6);
        Point p4 = new Point(1, 0);

        BoundingBox box = new BoundingBox(p1, p2, p3, p4);

        System.out.println("Max " + box.getMaxPoint());
        System.out.println("Min " + box.getMinPoint());
        System.out.println("Width " + box.getWidth());
        System.out.println("Height " + box.getHeight());
    }
}
